function showOver(title,txt){
  if(!document.getElementById){
    return;
  }
  else{
    overText(document.createTextNode(title),document.createTextNode(txt));
  }
}


function getPageX(e){
  var posx = 0;
  if (!e) var e = window.event;
  if (e.pageX){
    posx = e.pageX;
  }
  else if (e.clientX){
    posx = e.clientX;
  }
  if (document.body && document.body.scrollLeft){
    posx += document.body.scrollLeft;
  }
  else if (document.documentElement && document.documentElement.scrollLeft){
    posx += document.documentElement.scrollLeft;
  }
  return posx;
}


function getPageY(e){
  var posy = 0;
  if (!e) var e = window.event;
  if (e.pageY){
    posy = e.pageY;
  }
  else if (e.clientY){
    posy = e.clientY;
  }
  if (document.body && document.body.scrollTop){
    posy += document.body.scrollTop;
  }
  else if (document.documentElement && document.documentElement.scrollTop){
    posy += document.documentElement.scrollTop;
  }
  return posy;
}

function overText(contentNode,titleNode){
  if(titleNode!=null){
    var title=document.getElementById("overTitle");
    title.replaceChild(titleNode,title.firstChild);
  }
  var over=document.getElementById("overText");
  over.replaceChild(contentNode,over.firstChild);
  document.onmousemove=moveOver;
}

function moveOver(e){
  var over=document.getElementById("over");
  with(over.style){
    left=(getPageX(e)+10)+"px";
    top=(getPageY(e)+10)+"px";
    visibility='visible';
  }
}

function hideOver(){
  var over=document.getElementById("over");
  with(over){
    style.visibility="hidden";
    style.left="0px";
    style.top="0px";
  }
  document.onmousemove=null;
}
