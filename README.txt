over_text.module
README.txt

Description
------------

The over_text module enables the use of popup tooltip-like boxes using
an included javascript function, the wz_tooltip library, or, optionally,
the overlib library by Erik Bosrup (erik@bosrup.com) (a separate download
since its license, while open source, is not GPL).

The included overtext.js functions use DOM methods and are therefore
limited to more recent browsers (IE5+, NS6+, Moz 1+).  For earlier
browser version support, use the wz_tooltip or overlib library.

Over_text is similar to the existing "nicelinks" Drupal module.  For
many purposes, nicelinks is easier to use, as it automatically 
substitutes existing "title" attributes in page elements rather than
relying, as over_text does, on custom calls.  Over_text is useful in 
situations where one wants tooltips only for selected page content,
rather than everything that has a "title" attribute.

Usage
-----
The main usage of over_text is through a call to the function

  function over_text_make($title, $caption)

where the first parameter, $title, is the title of the box and the 
second, $caption, a text string to display as the caption; both 
variables are strings.  The function returns an array of attributes
ready to be plugged into a call to the l() link creation function.

For example:

  $params = over_text_make("This is the title", "This is the text");
  return l(t("Hello there"), "http://www.example.com", $params);

will create a link with the text "Hello there".  On hovering over the
link, a box will appear,
 __________________
|This is the title |
|------------------|
|This is the text  |
|__________________|

move on mouse move, and disappear on mouse out.

For more complex functionality, the functions can also be called
directly.  Call the overtext function directly (from Javascript, not
PHP) in the following way

  overText(contentNode,titleNode)

where both contentNode and titleNode are valid XHTML nodes.  This
allows the display of any type of content in popup boxes (e.g., a
table).  For direct calls on the wz_tooltip library see documentation
on the wz_tooltip website.

If you use the events module with a calendar block, over_text will
automatically generate popups for each event day showing the event 
title and description.

Configuration
-------------
 
To configure the module, click administration > configuration > modules
> over_text.  

The only setting is to select which library to use.  The included
overtext.js library is only 2k in size, but the wz_tooltip offers
greater support.


